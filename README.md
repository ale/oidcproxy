oidcproxy
=========

An *OpenID Connect*-enabled reverse proxy for HTTP-based services that
do not support authentication themselves. Used to protect legacy
services behind authentication. It can also apply ACLs using the
Warden API of [Hydra](https://github.com/ory/hydra).

Multiple domains can be configured separately, each with multiple
upstreams. When multiple upstreams are defined, the connections will
be load balanced among them (the current load balancing algorithm is
very naive: connections will be randomly balanced, trying to avoid
backends that return errors).

The program is configured with a YAML file, which by default should be
in `/etc/oidcproxy/oidcproxy.yml`. The following attributes are
supported:

`hydra_url`: Location of the Hydra server

`client_id`: OAuth client ID (main client)

`client_secret`: OAuth client ID (main client)

`session_auth_key`: A random 32-byte key, used to sign cookie-based
sessions

`session_enc_key`: A random 32-byte key, used to encrypt cookie-based
sessions

`backends`: List of supported domains

Each configured backend is again a dictionary with the following
attributes:

`host`: DNS name of this service. This will be matched against the
incoming request. Path matching is not supported.

`upstream`: List of upstream URLs

`client_id`: OAuth client ID (service-specific client)

`client_secret`: OAuth client secret (service-specific client)

`hydra_resource`: Resource to use for ACL checks in Hydra. If not
specified, any valid user will be allowed access to the service.

`client_tls`: TLS configuration for the client side

`server_tls`: TLS configuration for the server side (mandatory)

TLS configuration dictionaries allow the following attributes:

`cert`: Path to certificate file (PEM-encoded)

`key`: Path to private key file (PEM-encoded)

`ca`: Path to the CA
