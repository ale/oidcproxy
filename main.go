package main

import (
	"crypto/tls"
	"errors"
	"flag"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/coreos/go-oidc"
	"github.com/gorilla/sessions"
	"github.com/ory/hydra/sdk"
	"golang.org/x/net/context"
	"gopkg.in/yaml.v2"
)

var (
	addr          = flag.String("addr", ":3003", "address to listen on")
	configFile    = flag.String("config", "/etc/oidcproxy/oidcproxy.yml", "path of config file")
	insecureHttps = flag.Bool("insecure-https", false, "disable strict SSL verification")
)

func loadConfig() (*Configuration, error) {
	// Read YAML config.
	data, err := ioutil.ReadFile(*configFile)
	if err != nil {
		return nil, err
	}
	var config Configuration
	if err := yaml.Unmarshal(data, &config); err != nil {
		return nil, err
	}

	// Check required parameters.
	if config.HydraURL == "" {
		return nil, errors.New("hydra_url not set")
	}
	if config.ClientID == "" {
		return nil, errors.New("client_id not set")
	}
	if config.ClientSecret == "" {
		return nil, errors.New("client_secret not set")
	}
	if config.SessionAuthKey == "" {
		return nil, errors.New("session_auth_key not set")
	}
	if config.SessionEncKey == "" {
		return nil, errors.New("session_enc_key not set")
	}

	return &config, nil
}

// Set defaults for command-line flags using variables from the environment.
func setFlagDefaultsFromEnv() {
	flag.VisitAll(func(f *flag.Flag) {
		envVar := "OIDCPROXY_" + strings.ToUpper(strings.Replace(f.Name, "-", "_", -1))
		if value := os.Getenv(envVar); value != "" {
			f.DefValue = value
			f.Value.Set(value)
		}
	})
}

func main() {
	setFlagDefaultsFromEnv()
	flag.Parse()

	config, err := loadConfig()
	if err != nil {
		log.Fatal(err)
	}

	store = sessions.NewCookieStore([]byte(config.SessionAuthKey), []byte(config.SessionEncKey))

	hydra, err := sdk.Connect(
		sdk.ClusterURL(config.HydraURL),
		sdk.ClientID(config.ClientID),
		sdk.ClientSecret(config.ClientSecret),
		sdk.Scopes("hydra"),
		sdk.SkipTLSVerify(*insecureHttps),
	)
	if err != nil {
		log.Fatal(err)
	}

	ctx := context.Background()
	if *insecureHttps {
		ctx = oidc.ClientContext(ctx, &http.Client{
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{
					InsecureSkipVerify: true,
				},
			},
		})
	}
	oidcProvider, err := oidc.NewProvider(ctx, config.HydraURL)
	if err != nil {
		log.Fatal(err)
	}

	var backends []*backendHandler
	for _, bspec := range config.Backends {
		b, err := newBackendHandler(hydra, oidcProvider, config, bspec)
		if err != nil {
			log.Fatal(err)
		}
		backends = append(backends, b)
	}

	rooth := buildRootHandler(config, backends)
	tlsConfig, err := buildServerTLSConfig(config)
	if err != nil {
		log.Fatal(err)
	}
	srv := &http.Server{
		Addr:      *addr,
		Handler:   rooth,
		TLSConfig: tlsConfig,
	}
	if err := srv.ListenAndServeTLS("", ""); err != nil {
		log.Fatal(err)
	}
}
