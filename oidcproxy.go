package main

import (
	"crypto/rand"
	"crypto/tls"
	"crypto/x509"
	"encoding/gob"
	"encoding/hex"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"

	"github.com/coreos/go-oidc"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"github.com/ory/hydra/firewall"
	"github.com/ory/hydra/sdk"
	"golang.org/x/oauth2"
)

func init() {
	gob.Register(&oidc.IDToken{})
}

var store sessions.Store

const sessionName = "__pauth"

type backendHandler struct {
	*Backend

	verifier     *oidc.IDTokenVerifier
	oauth2Config *oauth2.Config
	hydra        *sdk.Client
	proxy        http.Handler
}

func newBackendHandler(hydra *sdk.Client, provider *oidc.Provider, config *Configuration, b *Backend) (*backendHandler, error) {
	// Setup upstream connections.
	if len(b.Upstream) < 1 {
		return nil, errors.New("no backends specified")
	}
	u := &url.URL{Scheme: "http", Host: b.Host}
	if b.ClientTLSConfig != nil {
		u.Scheme = "https"
	}
	proxy := httputil.NewSingleHostReverseProxy(u)

	var tlsConfig *tls.Config
	if b.ClientTLSConfig != nil {
		var err error
		tlsConfig, err = b.ClientTLSConfig.toClientConfig()
		if err != nil {
			return nil, err
		}
	}
	proxy.Transport = newTransport(b.Upstream, tlsConfig)

	// Setup OpenID Connect. Every backend needs its own client,
	// due to the restrictions imposed on the redirect URL
	// parameter.
	verifier := provider.Verifier(&oidc.Config{
		ClientID: b.ClientID,
	})
	oauth2Config := &oauth2.Config{
		ClientID:     b.ClientID,
		ClientSecret: b.ClientSecret,
		RedirectURL:  fmt.Sprintf("https://%s/callback", b.Host),
		Endpoint:     provider.Endpoint(),
		Scopes:       []string{oidc.ScopeOpenID},
	}

	return &backendHandler{
		Backend:      b,
		proxy:        proxy,
		hydra:        hydra,
		verifier:     verifier,
		oauth2Config: oauth2Config,
	}, nil
}

func (b *backendHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// Deal with OAuth callbacks separately.
	if r.URL.Path == "/callback" {
		b.handleCallback(w, r)
		return
	}

	// Wrap a request to the proxy.
	session, _ := store.Get(r, sessionName)
	token, ok := session.Values["id_token"].(*oidc.IDToken)
	if !ok || token == nil {
		nonce := makeUniqueNonce()
		session.Values["state"] = nonce
		session.Values["redir"] = r.URL.String()
		redir := b.oauth2Config.AuthCodeURL(nonce) + "&nonce=" + nonce
		sessions.Save(r, w)
		http.Redirect(w, r, redir, http.StatusFound)
		return
	}

	if b.HydraResource != "" {
		// Check ACLs for this user.
		err := b.hydra.Warden.IsAllowed(r.Context(), &firewall.AccessRequest{
			Subject:  token.Subject,
			Resource: b.HydraResource,
			Action:   "access",
		})
		if err != nil {
			log.Printf("access forbidden for %s to %s", token.Subject, r.URL.String())
			http.Error(w, err.Error(), http.StatusForbidden)
			return
		}
	}

	// Set the username as an HTTP header in the upstream request.
	r.Header.Set("X-Authenticated-User", token.Subject)

	b.proxy.ServeHTTP(w, r)
}

func (b *backendHandler) handleCallback(w http.ResponseWriter, r *http.Request) {
	// Verify state.
	session, _ := store.Get(r, sessionName)
	state, ok := session.Values["state"].(string)
	if !ok {
		http.Error(w, "No state", http.StatusBadRequest)
		return
	}
	if r.FormValue("state") != state {
		http.Error(w, "CSRF mismatch", http.StatusBadRequest)
		return
	}
	delete(session.Values, "state")

	// Create a context to talk to Hydra.
	ctx := r.Context()
	if *insecureHttps {
		ctx = oidc.ClientContext(ctx, &http.Client{
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{
					InsecureSkipVerify: true,
				},
			},
		})
	}

	// Exchange the code for a token.
	token, err := b.oauth2Config.Exchange(ctx, r.FormValue("code"))
	if err != nil {
		log.Printf("Exchange error: %v", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	if !token.Valid() {
		log.Printf("Received an invalid token from Exchange()")
		http.Error(w, "Invalid token", http.StatusBadRequest)
		return
	}

	// Extract the ID token and store it in our session.
	rawIDToken := token.Extra("id_token").(string)
	idToken, err := b.verifier.Verify(ctx, rawIDToken)
	if err != nil {
		log.Printf("Verify error: %v", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	session.Values["id_token"] = idToken
	redirURL, ok := session.Values["redir"].(string)
	if !ok {
		redirURL = "/"
	} else {
		delete(session.Values, "redir")
	}

	sessions.Save(r, w)
	http.Redirect(w, r, redirURL, http.StatusFound)
}

type Backend struct {
	Host     string   `yaml:"host"`
	Upstream []string `yaml:"upstream"`

	ClientID        string     `yaml:"client_id"`
	ClientSecret    string     `yaml:"client_secret"`
	HydraResource   string     `yaml:"hydra_resource"`
	ClientTLSConfig *TLSConfig `yaml:"client_tls"`
	ServerTLSConfig *TLSConfig `yaml:"server_tls"`
}

type TLSConfig struct {
	Cert string `yaml:"cert"`
	Key  string `yaml:"key"`
	CA   string `yaml:"ca"`
}

func (c *TLSConfig) toClientConfig() (*tls.Config, error) {
	cert, err := tls.LoadX509KeyPair(c.Cert, c.Key)
	if err != nil {
		return nil, err
	}

	cas, err := loadCA(c.CA)
	if err != nil {
		return nil, err
	}

	return &tls.Config{
		Certificates: []tls.Certificate{cert},
		RootCAs:      cas,
	}, nil
}

func loadCA(path string) (*x509.CertPool, error) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	cas := x509.NewCertPool()
	cas.AppendCertsFromPEM(data)
	return cas, nil
}

func buildServerTLSConfig(config *Configuration) (*tls.Config, error) {
	var certs []tls.Certificate
	for _, b := range config.Backends {
		cert, err := tls.LoadX509KeyPair(b.ServerTLSConfig.Cert, b.ServerTLSConfig.Key)
		if err != nil {
			return nil, err
		}
		certs = append(certs, cert)
	}

	c := &tls.Config{
		Certificates: certs,
	}

	if config.CA != "" {
		cas, err := loadCA(config.CA)
		if err != nil {
			return nil, err
		}
		c.ClientAuth = tls.RequireAndVerifyClientCert
		c.ClientCAs = cas
	}

	c.BuildNameToCertificate()

	return c, nil
}

type Configuration struct {
	HydraURL       string `yaml:"hydra_url"`
	ClientID       string `yaml:"client_id"`
	ClientSecret   string `yaml:"client_secret"`
	SessionAuthKey string `yaml:"session_auth_key"`
	SessionEncKey  string `yaml:"session_enc_key"`
	CA             string `yaml:"ca"`

	Backends []*Backend `yaml:"backends"`
}

func buildRootHandler(config *Configuration, backends []*backendHandler) http.Handler {
	r := mux.NewRouter()
	for _, b := range backends {
		r.Host(b.Host).Handler(b)
	}
	return r
}

func makeUniqueNonce() string {
	var b [16]byte
	rand.Read(b[:])
	return hex.EncodeToString(b[:])
}
