package main

import (
	"crypto/tls"
	"errors"
	"log"
	"math/rand"
	"net"
	"net/http"
	"sort"
)

func resolveIPs(hosts []string) []string {
	var resolved []string
	for _, hostport := range hosts {
		host, port, err := net.SplitHostPort(hostport)
		if err != nil {
			log.Printf("error parsing %s: %v", hostport, err)
			continue
		}
		hostIPs, err := net.LookupIP(host)
		if err != nil {
			log.Printf("error resolving %s: %v", host, err)
			continue
		}
		for _, ip := range hostIPs {
			resolved = append(resolved, net.JoinHostPort(ip.String(), port))
		}
	}
	return resolved
}

type balancer struct {
	ips  []string
	errs []uint64
}

func (b *balancer) dial(network, addr string) (net.Conn, error) {
	ips, err := b.pickIPs()
	if err != nil {
		return nil, err
	}
	for _, s := range ips {
		conn, err := net.Dial(network, s.ip)
		if err == nil {
			return conn, nil
		}
		log.Printf("error connecting to %s: %v", s.ip, err)
		b.errs[s.index]++
	}
	return nil, errors.New("all upstream connections failed")
}

type ipScore struct {
	ip    string
	score int
	index int
}

type ipScoreList []ipScore

func (l ipScoreList) Len() int           { return len(l) }
func (l ipScoreList) Swap(i, j int)      { l[i], l[j] = l[j], l[i] }
func (l ipScoreList) Less(i, j int) bool { return l[i].score < l[j].score }

func shuffleScores(scores []ipScore) {
	for i, j := range rand.Perm(len(scores)) {
		scores[i], scores[j] = scores[j], scores[i]
	}
}

const minErrs = 3

func (b *balancer) pickIPs() ([]ipScore, error) {
	scores := make([]ipScore, len(b.ips))
	for i, ip := range b.ips {
		score := 1
		if b.errs[i] > minErrs {
			score *= 10
		}
		scores[i] = ipScore{ip: ip, score: score, index: i}
	}

	sort.Sort(ipScoreList(scores))

	// Iterate through the sorted list, shuffling groups of
	// elements that have identical scores.
	curScore := scores[0].score
	head := 0
	for i := 1; i < len(scores); i++ {
		if scores[i].score != curScore {
			group := scores[head : i+1]
			if len(group) > 1 {
				shuffleScores(group)
			}
			head = i + 1
		}
	}
	group := scores[head:len(scores)]
	if len(group) > 1 {
		shuffleScores(group)
	}

	return scores, nil
}

func newTransport(backends []string, tlsConf *tls.Config) http.RoundTripper {
	ips := resolveIPs(backends)
	b := &balancer{
		ips:  ips,
		errs: make([]uint64, len(ips)),
	}

	return &http.Transport{
		Dial:            b.dial,
		TLSClientConfig: tlsConf,
	}
}
